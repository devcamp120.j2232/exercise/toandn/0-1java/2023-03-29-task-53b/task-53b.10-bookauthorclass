import module.*;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nguyen Van A","a@gmail.com",'m');
        System.out.println("Author 1:");
        System.out.println(author1);

        Author author2 = new Author("Lai Thi B","b@gmail.com",'f');
        System.out.println("Author 2:");
        System.out.println(author2);

        Book book1 = new Book("Clean code", author1, 10000, 1);
        System.out.println("Book 1:");
        System.out.println(book1);

        Book book2 = new Book("Yoga", author2, 12000, 2);
        System.out.println("Book 2:");
        System.out.println(book2);
    }
}
